package com.labs.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.labs.bo.Person;
import com.labs.repository.PersonRepository;

@RestController
@CacheConfig(cacheNames = "people")
public class PersonWS {

	@Autowired
	private PersonRepository repository;

	@RequestMapping("/persons/")
	@Cacheable
	public List<Person> getPersons() {
		List<Person> persons = repository.findAll();
		return persons;

	}

}
