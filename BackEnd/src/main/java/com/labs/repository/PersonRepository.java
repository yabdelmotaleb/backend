package com.labs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.labs.bo.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

}
