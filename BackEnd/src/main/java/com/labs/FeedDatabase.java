package com.labs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.labs.bo.Contract;
import com.labs.bo.Person;
import com.labs.repository.PersonRepository;

@Component
public class FeedDatabase implements ApplicationRunner {

	@Autowired
	private PersonRepository repository;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Person person = new Person("Jack", "Bauer");
		List<Contract> contracts = new ArrayList<Contract>();
		contracts.add(new Contract(1, "contract1", person));
		contracts.add(new Contract(2, "contract2", person));
		person.setContracts(contracts);
		//repository.save(person);

		Person person2 = new Person("Jack2", "Bauer2");
		List<Contract> contracts2 = new ArrayList<Contract>();
		contracts2.add(new Contract(1, "contract21", person2));
		contracts2.add(new Contract(2, "contract22", person2));
		person2.setContracts(contracts2);
		//repository.save(person2);

	}

}
